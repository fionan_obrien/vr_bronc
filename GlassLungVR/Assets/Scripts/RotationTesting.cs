﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationTesting : MonoBehaviour {
    /*
     * 
     * Utility class to help undesrtand Quaterionn rotation on a specific axis 
     * 
     * 
     
         */
    public GameObject other;
    public float t;

    Vector3 oldPosition;
    Vector3 oldTargetPosition;

    Quaternion to, from;

    public float speed;
	// Use this for initialization
	void Start () {

        lockOn();
	}
	
	// Update is called once per frame
	void Update () {
        Quaternion q;
        if (t < 1.0f)
        {
            q = Quaternion.Slerp(from, to, t); 
            t += Time.deltaTime;
            transform.rotation = q;


        }
        if(oldPosition != transform.position)
        {

           
            t = 0;
            lockOn();

        }
        if(oldTargetPosition != other.transform.position)
        {

            t = 0;
            lockOn();
        }


	}


    void lockOn() {
        Vector3 toOther = other.transform.position - transform.position;
        toOther.Normalize();
        Vector3 axis = Vector3.Cross(Vector3.forward, toOther);

        float angle = Mathf.Acos(Vector3.Dot(Vector3.forward, toOther));
        to = Quaternion.AngleAxis(angle * Mathf.Rad2Deg, axis);

        from = transform.rotation;
        t = 0;

        oldPosition = transform.position;
        oldTargetPosition = other.transform.position;
    }

}
