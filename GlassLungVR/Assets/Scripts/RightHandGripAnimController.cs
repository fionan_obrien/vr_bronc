﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RightHandGripAnimController : MonoBehaviour {

    /*
     * Animation Script for righthand grip animation
     * 
     * */

    public string RightGripButton = "rightGrip";
    // Use this for initialization

    private Animator anim;

    bool isGripping() { return Input.GetAxis(RightGripButton)> .5f; }


    private void Start()
    {
        anim = GetComponent<Animator>();
    }

    private void Update()
    {



        if (isGripping())
        {

            anim.SetBool("Gripping" ,true);

        }else
        {
            anim.SetBool("Gripping", false);

        }
    }

}
