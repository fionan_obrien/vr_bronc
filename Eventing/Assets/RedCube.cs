﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedCube : MonoBehaviour {


    private void OnEnable()
    {
        EventManager.movmentEvt += MoveUp;
    }

    private void OnDisable()
    {
        EventManager.movmentEvt -= MoveUp;
    }
    void MoveUp(bool dir) {
        if (dir) {

          transform.position += transform.up;
        }else {

            transform.position -= transform.up;

        }
    }

}
