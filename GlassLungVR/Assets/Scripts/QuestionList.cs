﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestionList : IEnumerable
{

    private string FileName;


    public delegate void NewQuestion(Question.QuestionType type);
    public static event NewQuestion newQuestion;



    public delegate void QuestionsCompleted();
    public static event QuestionsCompleted questionsComplete;

    ArrayList availableQuestions;
    Dictionary<Question,string> questions_and_answers;
    List<Question> redundantQuestions;
    int pos;

    

   public Question current;

    public QuestionList(string filename) {
        FileName = filename;

        getAllQuestions();
        pos = 0;
        
    }

    public void answerQuestion(string answer) {

        if (current != null) {

        bool isExpected = current.evaluate(answer);

          
            if (!isExpected)
        {
 
                removeRedudant(current.getId());
              
            }
       
        }
        getNextQuestion();
        
    }


   

    private void removeRedudant(int id) {

        for (int i = 0; i < availableQuestions.Count; i++)
        {
            Question q = (Question)availableQuestions[i];
                
                int id2 = q.getId();
           


                if (q.isChildOf(id))
            {
               

               // removeRedudant(id2);
          
                availableQuestions.Remove(q);
            }
            
        }
       
    }
    
    public void getNextQuestion()
    {   if(pos>= availableQuestions.Count)
        {
    
            questionsComplete();
            return;
        }
        pos = availableQuestions.IndexOf(current);
        pos++;

        if (pos >= availableQuestions.Count)
        {
            questionsComplete();
      
            return;
        }
        current =(Question) availableQuestions[pos];
        newQuestion(current.type);

    }



    private void getAllQuestions()
    {

          availableQuestions = new ArrayList();
        if (System.IO.File.Exists(FileName))
        {
           string[] lines= System.IO.File.ReadAllLines(FileName);
           
            foreach(string line in lines)
            {
                Debug.Log(line);
                if (line.Length < 5) break;

                Question q = Question.parseForQuestion(line);
                if (q != null)
                {
                    availableQuestions.Add(q);
                }
            }

        }
        else {
            Debug.LogError("Questions File not Found");
        }

  

    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        foreach (Question q in availableQuestions)
        {
            if (q == null) { break; }
            yield return q;

        }
    }
}
