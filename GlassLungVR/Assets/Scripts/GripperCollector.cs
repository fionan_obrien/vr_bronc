﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GripperCollector : MonoBehaviour {

    // Use this for initialization
    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("AH Collision!!");
        Debug.Log(collision.gameObject.name);

    }

    public delegate void OnGripperCollect();
    public static event OnGripperCollect gripperCollect;


    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Yo've Been Hit by... a smooth" + gameObject.name);
        Destroy(other.gameObject);
        gripperCollect();


    }
}
