﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftGripAnimController : MonoBehaviour {

    /*
     * Animation script for Left Hand Grip animation
     * 
     * */

    private Animator anim;


    private void Start()
    {
        anim = GetComponent<Animator>();
    }

    private void OnEnable()
    {
        GripMovementByVelocity.leftgripAction += GripMovementByVelocity_leftgripAction;
    }

    private void OnDisable()
    {
        GripMovementByVelocity.leftgripAction -= GripMovementByVelocity_leftgripAction;
    }


    private void GripMovementByVelocity_leftgripAction(bool grip)
    {
        anim.SetBool("Gripping", grip);
    }
}
