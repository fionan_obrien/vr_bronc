﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UseGripperOnClick : MonoBehaviour {

    public delegate void OnGripperActionPress();
    public static event OnGripperActionPress gripperActionPress;


    public GameObject gripperGameObject;
    public string use_gripper_button;
    float timeActive = 2f;


	// Use this for initialization
	void Start () {
        gripperGameObject.SetActive(false);
	}


    private void Update()
    {
        if(Input.GetAxis(use_gripper_button)> .9f)
        {


            if (!gripperGameObject.activeSelf)
            {
                gripperGameObject.SetActive(true);

                gripperActionPress();
                StartCoroutine(LateCall());


            }

        }
    }
    IEnumerator LateCall()
    {

        yield return new WaitForSeconds(timeActive);

        gripperGameObject.SetActive(false);
       
    }

}
