﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour {


    public delegate void SomeDelegate(bool dir);// define the method sig
    public static SomeDelegate movmentEvt ; //define event
    public event SomeDelegate nips;

    public static void MyEvent(bool dir) {

        if (movmentEvt != null) {

            movmentEvt(dir);
        }
    }

    protected virtual void onNips() {
        if (movmentEvt != null)
        {

        }

    }

}
