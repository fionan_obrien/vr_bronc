﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementAudio : MonoBehaviour {
    public AudioClip soundToPlay;
    public AudioSource audio;
    public float volume;
    private Vector3 lastPos;
    bool moving;
    public  GameObject host;

    // Use this for initialization
    void Start () {
        lastPos = host.transform.position;

	}
	
	// Update is called once per frame
	void Update () {

        if (lastPos != host.transform.position)
        {

            moving = true;
            lastPos = host.transform.position;
        }
        else {
            moving = false;
            lastPos = host.transform.position;
        }

        if (moving)
        {

            playSound();


        }
        else
        {

            audio.Stop();

        }

	}

    private void playSound() {
        if (audio.isPlaying) {

        }
        else { audio.Play(); }
        

    }
}
