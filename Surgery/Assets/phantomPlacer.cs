﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class phantomPlacer : MonoBehaviour {

    public GameObject phantom;
    private Vector3 midpoint;

	// Use this for initialization
	void Start () {

        Vector3 centroid = new Vector3(0, 0, 0);

        if (transform.childCount > 0) {

            Transform[] allchildren = transform.gameObject.GetComponentsInChildren<Transform>();
            
            foreach (Transform trans in allchildren) {

                centroid += trans.transform.position;

            }
            centroid /= allchildren.Length;
        }
        
        Instantiate(phantom, centroid, transform.rotation);
        
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
