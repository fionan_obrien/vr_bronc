var searchData=
[
  ['camerarig',['CameraRig',['../class_o_v_r_debug_head_controller.html#a799cde14aea9f03f07a4c41e4eaf21e9',1,'OVRDebugHeadController.CameraRig()'],['../class_o_v_r_player_controller.html#a318e0aacfc88250eb9dd7ed6123d6d15',1,'OVRPlayerController.CameraRig()']]],
  ['capacity',['Capacity',['../class_o_v_r_haptics_clip.html#a62d905e95b811d0a7e98621cc81be876',1,'OVRHapticsClip']]],
  ['centereyeanchor',['centerEyeAnchor',['../class_o_v_r_camera_rig.html#a2716fdeda387531693c0c1f83b7a1b57',1,'OVRCameraRig']]],
  ['change',['change',['../class_scene_shift.html#a096f68c1a88800b89406af650e0adb6b',1,'SceneShift']]],
  ['channels',['Channels',['../class_o_v_r_haptics.html#ad1bdf6585fb41764477a4c683a31ae8b',1,'OVRHaptics']]],
  ['checkforgraborrelease',['CheckForGrabOrRelease',['../class_o_v_r_grabber.html#a2323d97b994c20a8d2205b067ee9313a',1,'OVRGrabber']]],
  ['chromatic',['chromatic',['../class_o_v_r_manager.html#a03d8edcf01fa76af5a66d4441bef913a',1,'OVRManager']]],
  ['clampangle',['ClampAngle',['../class_mouse_look.html#a595b7f9057fd0eb90fd0556742cb6aaa',1,'MouseLook']]],
  ['clear',['Clear',['../class_o_v_r_haptics_1_1_o_v_r_haptics_channel.html#ab9c6978a466a5d43e6834f6c68e72dff',1,'OVRHaptics::OVRHapticsChannel']]],
  ['closestdistance',['ClosestDistance',['../struct_o_v_r_boundary_1_1_boundary_test_result.html#aad3eb4c22811c6f535f69316ba3a8eb6',1,'OVRBoundary.BoundaryTestResult.ClosestDistance()'],['../struct_o_v_r_plugin_1_1_boundary_test_result.html#a8489554273b68da71243f9108050233e',1,'OVRPlugin.BoundaryTestResult.ClosestDistance()']]],
  ['closestpoint',['ClosestPoint',['../struct_o_v_r_boundary_1_1_boundary_test_result.html#afd1b04200644c1a6ff9197e5e82c6f4b',1,'OVRBoundary.BoundaryTestResult.ClosestPoint()'],['../struct_o_v_r_plugin_1_1_boundary_test_result.html#a3fa113cec67e088ff3183589da19979d',1,'OVRPlugin.BoundaryTestResult.ClosestPoint()']]],
  ['closestpointnormal',['ClosestPointNormal',['../struct_o_v_r_boundary_1_1_boundary_test_result.html#ae7c17af022e8ff3d3f928b3d2198d5bf',1,'OVRBoundary.BoundaryTestResult.ClosestPointNormal()'],['../struct_o_v_r_plugin_1_1_boundary_test_result.html#adf12b0dd65735483f04aba0cf0ecb359',1,'OVRPlugin.BoundaryTestResult.ClosestPointNormal()']]],
  ['collectedaction',['CollectedAction',['../class_collected_action.html',1,'']]],
  ['collectedaction_2ecs',['CollectedAction.cs',['../_collected_action_8cs.html',1,'']]],
  ['collectme',['CollectMe',['../class_collect_me.html',1,'']]],
  ['collector_2ecs',['Collector.cs',['../_collector_8cs.html',1,'']]],
  ['collectvalue',['collectvalue',['../class_score_manager.html#aad972d8d09beae2bffc51cf0b1c7a2aa',1,'ScoreManager']]],
  ['color',['Color',['../struct_o_v_r_boundary_1_1_boundary_look_and_feel.html#a4f37c11c4897623e0542262c9c7c0101',1,'OVRBoundary.BoundaryLookAndFeel.Color()'],['../struct_o_v_r_plugin_1_1_boundary_look_and_feel.html#a5a19d376a7f88a25245569910fef9ee2',1,'OVRPlugin.BoundaryLookAndFeel.Color()']]],
  ['colorf',['Colorf',['../struct_o_v_r_plugin_1_1_colorf.html',1,'OVRPlugin']]],
  ['compositorcpuelapsedtime',['CompositorCpuElapsedTime',['../struct_o_v_r_plugin_1_1_app_perf_frame_stats.html#aa079560cfb2395d001885da92efc0942',1,'OVRPlugin::AppPerfFrameStats']]],
  ['compositorcpustarttogpuendelapsedtime',['CompositorCpuStartToGpuEndElapsedTime',['../struct_o_v_r_plugin_1_1_app_perf_frame_stats.html#a3c8e2cbcf4eb9e85aa7622cdfb3f4178',1,'OVRPlugin::AppPerfFrameStats']]],
  ['compositordroppedframecount',['CompositorDroppedFrameCount',['../struct_o_v_r_plugin_1_1_app_perf_frame_stats.html#a6ca2399c4f74bf0f795b771130fd66dd',1,'OVRPlugin::AppPerfFrameStats']]],
  ['compositorframeindex',['CompositorFrameIndex',['../struct_o_v_r_plugin_1_1_app_perf_frame_stats.html#a7669d0901d5b8ef78764d6efc998b2b6',1,'OVRPlugin::AppPerfFrameStats']]],
  ['compositorgpuelapsedtime',['CompositorGpuElapsedTime',['../struct_o_v_r_plugin_1_1_app_perf_frame_stats.html#a8fcd175f0abb457d1c6c8a79f292c7a1',1,'OVRPlugin::AppPerfFrameStats']]],
  ['compositorgpuendtovsyncelapsedtime',['CompositorGpuEndToVsyncElapsedTime',['../struct_o_v_r_plugin_1_1_app_perf_frame_stats.html#a301b178941a55d73f7e6ffc5ff6d8482',1,'OVRPlugin::AppPerfFrameStats']]],
  ['compositorlatency',['CompositorLatency',['../struct_o_v_r_plugin_1_1_app_perf_frame_stats.html#a48980767c9cd3566284ef9c58d0b5fcc',1,'OVRPlugin::AppPerfFrameStats']]],
  ['config',['Config',['../class_o_v_r_haptics_1_1_config.html',1,'OVRHaptics']]],
  ['configuration',['Configuration',['../class_doxygen_window.html#ad1f6043062e30f52cb634b72294a5676a254f642527b45bc260048e30704edb39',1,'DoxygenWindow']]],
  ['connectedcontrollers',['ConnectedControllers',['../struct_o_v_r_plugin_1_1_controller_state.html#a151c990aea6885ca42746afd75b7a437',1,'OVRPlugin::ControllerState']]],
  ['contorller',['Contorller',['../classrotate__scope.html#a6a2569dcd3bded3b4930d86f8d7b3686',1,'rotate_scope.Contorller()'],['../class_touch_controller.html#ac1a8d4101bafd4e027502e8d7823797d',1,'TouchController.Contorller()']]],
  ['controller',['controller',['../class_grab.html#ac55bc8cb780494935a6e16c50484fd73',1,'Grab.controller()'],['../class_o_v_r_input.html#a5c86f9052a9cbb0b73779ff5704d60a8',1,'OVRInput.Controller()'],['../class_o_v_r_player_controller.html#af34ed81e708c19b69f648611da69b093',1,'OVRPlayerController.Controller()'],['../class_grip_movement.html#a90c466173a8b65da1d14a90df08b2bcf',1,'GripMovement.Controller()'],['../classrotate__scope2.html#aa89ed339ec367a5d1137d630b789a78c',1,'rotate_scope2.Controller()']]],
  ['controllerstate',['ControllerState',['../struct_o_v_r_plugin_1_1_controller_state.html',1,'OVRPlugin']]],
  ['count',['Count',['../class_o_v_r_haptics_clip.html#a8ff02c899a70b250177d6e5da3608b2a',1,'OVRHapticsClip.Count()'],['../class_o_v_r_tracker.html#a2ad2fc923266a960417415c2518b8d53',1,'OVRTracker.count()']]],
  ['cpulevel',['cpuLevel',['../class_o_v_r_manager.html#a8bf65c129ac6fa1ffb6a00e406bb20b2',1,'OVRManager']]],
  ['create',['Create',['../class_o_v_r_touchpad.html#a61bda60c75797c792669466cd80ba8fa',1,'OVRTouchpad']]],
  ['cubemap',['Cubemap',['../class_o_v_r_overlay.html#a66270f911c2b6bdadf21de93ff3ab939a29b142520cd0349c9f7375be4aa848c0',1,'OVROverlay']]],
  ['cubemapsize',['cubemapSize',['../class_o_v_r_cubemap_capture.html#a8b1fee3c474c5302fe4f200d81ad7eaa',1,'OVRCubemapCapture']]],
  ['curentoutput',['CurentOutput',['../class_doxygen_window.html#a82b41ae2e3c44b050acc7603031ccd55',1,'DoxygenWindow']]],
  ['current',['current',['../class_question_list.html#a465db49229911ae6ed67f1095b449e05',1,'QuestionList']]],
  ['currentoverlayshape',['currentOverlayShape',['../class_o_v_r_overlay.html#a600ef0d2e01a4645f16d8c74a9b7c66b',1,'OVROverlay']]],
  ['currentoverlaytype',['currentOverlayType',['../class_o_v_r_overlay.html#ad972430849b375de2120afb8d5720701',1,'OVROverlay']]],
  ['cylinder',['Cylinder',['../class_o_v_r_overlay.html#a66270f911c2b6bdadf21de93ff3ab939a2ec2c2961c7ce5a114d969c1f562a563',1,'OVROverlay']]]
];
