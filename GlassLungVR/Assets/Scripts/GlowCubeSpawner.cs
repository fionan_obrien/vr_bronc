﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlowCubeSpawner : MonoBehaviour {


    /*
     * This class spawns the GlowCube GameOject in accoradance to the current questions requirements. 
     * 
     * 
     * 
     */

    public GameObject glow_cube;
    public float padding;
    private float offSet = 0f;

    List<GameObject> activeCubes;
    private void Start()
    {
        activeCubes = new List<GameObject>();
    }

    private void OnEnable()
    {
        QuestionList.newQuestion += QuestionList_newQuestion;
        GlowCube.update += GlowCube_update;
    }

    private void GlowCube_update(int value)
    {
        foreach(GameObject go in activeCubes)
        {

            GlowCube gc = go.GetComponent<GlowCube>();

            if (gc.getValue() <= value)
            {

                gc.setActive(true);
            }else {
                gc.setActive(false);
            }

        }
    }

    private void OnDisable()
    {
        QuestionList.newQuestion += QuestionList_newQuestion;
        GlowCube.update -= GlowCube_update;
    }

    private void QuestionList_newQuestion(Question.QuestionType type)
    {
        offSet = 0f;

        if (activeCubes.Count > 0)
        {
            GlowCube.resetCounter();
            foreach(GameObject g in activeCubes)
            {
                Destroy(g);
                
            }
            activeCubes.Clear();
        }

        int requiredCube = 0;

        switch (type)
        {
            case Question.QuestionType.binary:
                requiredCube = 2;
                break;
            case Question.QuestionType.qualitive:
                requiredCube = Question.qualitives.Length;
                break;
            case Question.QuestionType.rating:
                requiredCube = Question.ratings.Length;
                break;

        }

        for (int i = 0; i < requiredCube; i++)
        {
        GameObject go = GameObject.Instantiate(
            glow_cube,this.transform.position+ (this.transform.right *offSet), this.transform.rotation);
        offSet += go.GetComponent<Renderer>().bounds.size.y + padding;
            activeCubes.Add(go);
        }

    }
}
