﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnTutorialUpdateChange : MonoBehaviour {

    /*
     * TA Utility Class allowing for a tutorial mode that can swtich on and off other gameobjects throughout the scene easily
     * 
     * */


    public delegate void tutorialUpdate(string s, bool b);
    public static event tutorialUpdate tutUpdate;


    public int secondsTillBeep;

    private float beep;
    private void Start()
    {
        beep = 0f;
    }

    private void Update()
    {


        beep += Time.deltaTime;


        if(beep > secondsTillBeep)
        {

            //tutUpdate("MouseLook",true);
            //tutUpdate("KeyMovement", false);
            //tutUpdate("rotate_scope2", false);

        }


    }
}
