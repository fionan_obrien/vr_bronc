var searchData=
[
  ['default',['Default',['../class_o_v_r_manager.html#afe3ed999096f254b2b7e5c2329a06f41a7a1920d61156abc05a60135aefe8bc67',1,'OVRManager']]],
  ['double_5ftap',['DOUBLE_TAP',['../class_o_v_r_platform_menu.html#a9bebd8bd7984fcaac5ebdb64c2be8b9ea69262b7d2d5f239bbdeff40c30d17662',1,'OVRPlatformMenu']]],
  ['down',['Down',['../class_o_v_r_input.html#aed3cf5b4b5e0669cea0941f61e018ee5a08a38277b0309070706f6652eeae9a53',1,'OVRInput.Down()'],['../class_o_v_r_touchpad.html#a787fd5de32818ff630a69a460195c623a08a38277b0309070706f6652eeae9a53',1,'OVRTouchpad.Down()'],['../class_o_v_r_touchpad.html#a89545a52f6440bc85ef5b5549b45b02aa08a38277b0309070706f6652eeae9a53',1,'OVRTouchpad.Down()']]],
  ['dpaddown',['DpadDown',['../class_o_v_r_input.html#aed3cf5b4b5e0669cea0941f61e018ee5af926cb67913be40219a97ec3889f90d9',1,'OVRInput.DpadDown()'],['../class_o_v_r_input.html#a9d6423af820e22b93f0b33a4fc4bf77aaf926cb67913be40219a97ec3889f90d9',1,'OVRInput.DpadDown()']]],
  ['dpadleft',['DpadLeft',['../class_o_v_r_input.html#aed3cf5b4b5e0669cea0941f61e018ee5a126eb11514ec5936a690e24fce06a075',1,'OVRInput.DpadLeft()'],['../class_o_v_r_input.html#a9d6423af820e22b93f0b33a4fc4bf77aa126eb11514ec5936a690e24fce06a075',1,'OVRInput.DpadLeft()']]],
  ['dpadright',['DpadRight',['../class_o_v_r_input.html#aed3cf5b4b5e0669cea0941f61e018ee5ad3c7cfd48048567e4d093aaad0bbcb7a',1,'OVRInput.DpadRight()'],['../class_o_v_r_input.html#a9d6423af820e22b93f0b33a4fc4bf77aad3c7cfd48048567e4d093aaad0bbcb7a',1,'OVRInput.DpadRight()']]],
  ['dpadup',['DpadUp',['../class_o_v_r_input.html#aed3cf5b4b5e0669cea0941f61e018ee5ad3ca4590094b47dd5e7ddfd4679a1139',1,'OVRInput.DpadUp()'],['../class_o_v_r_input.html#a9d6423af820e22b93f0b33a4fc4bf77aad3ca4590094b47dd5e7ddfd4679a1139',1,'OVRInput.DpadUp()']]]
];
