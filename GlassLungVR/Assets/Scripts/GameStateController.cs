﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStateController : MonoBehaviour {

    /*
    * This class is the only GameObject used to transfer GameState between Game Scenes
    * It produces the GUID https://msdn.microsoft.com/en-us/library/system.guid.newguid(v=vs.110).aspx
    * to produce unique game reference numbers.
    * 
    * */

    public string filename;

    public string Name { get; set; }
    public string GameNumber { get; set; }
    public string TimeStart { get; set; }


    System.IO.StreamWriter writer;


    private void OnEnable()
    {
        SceneShift.GameOver += SceneShift_GameOver;
    }

    private void OnDisable()
    {
        SceneShift.GameOver -= SceneShift_GameOver;
    }

    private void SceneShift_GameOver()
    {
        Debug.Log("Scene Change!");
    }

    private void Start()
    {
        DontDestroyOnLoad(this);
        TimeStart = string.Format("{0:HH:mm:ss tt}", System.DateTime.Now);

        Name = System.Guid.NewGuid().ToString();

        GameNumber = getGameCount();
    }

    
    private string getGameCount()
    {

        string gameNum;
        int gameasInt = 0;

        if (System.IO.File.Exists(filename))
        {
            string[] lines = System.IO.File.ReadAllLines(filename);

                int p = -1;
            System.Int32.TryParse(lines[0], out p);
            if (p != -1)
            {
                gameasInt = p;
            }

            

        }
        else
        {
            Debug.Log("GameCount File Not found");
            Debug.Log("First Time Load");
            gameasInt = 0;
        }
        gameNum = ""+ ++gameasInt;

        updateGameCount(gameasInt);

        return gameNum;


       

    }

    private void updateGameCount(int num) {

        writer = new System.IO.StreamWriter(filename, false);
        writer.AutoFlush = true;

        writer.WriteLine("" + num);
        writer.Close();
    }
}
