﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VectorMath : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    float distanceBetween(Vector3 a,Vector3 b)
    {

        float distance = 0f;

        distance += coordDistance(a.x, b.x);
        distance += coordDistance(a.y, b.y);
        distance += coordDistance(a.z, b.z);

        return distance;
    }

    Vector3 VectorDistanceBetween(Vector3 a, Vector3 b) {

        return new Vector3(coordDistance(a.x, b.x), coordDistance(a.y, b.y), coordDistance(a.z, b.z));

    }


    float coordDistance(float a, float b) {
        float distance = 0f;

        if (a >=0 && b >= 0)
        {
            //both over zero
            if (a >= b)
            {
                distance += a - b;
            }else
            {
                distance += b - a;
            }


        }else if(b < 0) {
            //b less than zero
            if (a < 0)
            {
                // both less than zero
                if (a >= b)
                {
                    distance += a - b;
                }
                else
                {
                    distance += (b - a)*-1;
                }

            }
            else {
                //only b less than zero
                distance += (a + (b * -1));
            }

        } else {
            //only a less than zero
            distance += (b + (a * -1));
        }



        return distance;
    }

}
