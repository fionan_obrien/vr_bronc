﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLog : MonoBehaviour {

    // Use this for initialization

    System.IO.StreamWriter writer;


    /*
     * This class writes CSV values of gameObjects being monitored.
     * It listens for adhoc events e.g hitting a wall, or gameover
     * It updates  in realtime  for passive event monitoring e.g position and rotation over time.
     * 
     * */

    public string filename;
    public  GameObject subject;
    public  bool monitoring;
   
    public string game_num;
    public int frequency;

    private  float ticker;
    private float tickerlimit;
    private Vector3 location;
    private Quaternion rotation;



    public bool is_Logging_position,
        is_Logging_rotation, 
        is_Logging_wallhit,
        is_Logging_score, 
        is_Logging_restart;


    enum Log_Types{POS,ROT,WHIT,SCORE,RSTART,PLYR};

    private void OnEnable()
    {
        ScoreManager.saveScore += ScoreManager_saveScore;
        OnWallHit.wallhit += OnWallHit_wallhit;
    }


    private void OnDisable()
    {
        ScoreManager.saveScore -= ScoreManager_saveScore;
        OnWallHit.wallhit -= OnWallHit_wallhit;
    }

   

    void Start () {
        writer = new System.IO.StreamWriter(filename, true);
        writer.AutoFlush = true;
        ticker = 0f;
        tickerlimit = 1.0f / frequency;

        GameStateController controller = FindObjectOfType<GameStateController>();
        game_num = controller.GameNumber;

	}
	
	// Update is called once per frame
	void Update () {
      
     
        if (monitoring )
        {
            ticker += Time.deltaTime;
            if (ticker > tickerlimit)
            {

                if (is_Logging_position) {

                    log_rotation();
                }
                if (is_Logging_rotation) {

                    log_position();
                }

                ticker = 0f;
            }
           

        }
	}
    private void log(params object[] data) {

        string  dat = game_num+","+Time.deltaTime;
            foreach(object s in data){
            dat += "," + s.ToString();
        }
        writer.WriteLine(dat);

    }

    private void log_rotation() {
        rotation = subject.transform.rotation;
        log(Log_Types.ROT, rotation.x,rotation.y,rotation.z,rotation.w);
    }
    private void log_position() {

        location = subject.transform.position;
        log(Log_Types.POS, location.x, location.y, location.z);
    }

    private void log_playerChange()
    {

        

    }

    private void log_restart() {
        //For future Use only
    }

    private void log_score(string playerName,string score) {
        log(Log_Types.SCORE, playerName, score);

    }
    private void log_wallhit(Vector3 hitLocation) {

        log(Log_Types.WHIT, hitLocation.x, hitLocation.y, hitLocation.z);
    }


    private void OnWallHit_wallhit(GameObject o,Vector3 position)
    {
        if (is_Logging_wallhit)
        {
            log_wallhit(position);

        }
    }


    private void ScoreManager_saveScore(string playerName, string score)
    {
        if (is_Logging_score)
        {
            log_score(playerName, score);
        }
       
    }


}
