﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotate_scope : MonoBehaviour {

    public OVRInput.Controller Contorller;
   
    public float rotateSpeed;
    public string ButtonName;
    public string joystickButton;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        //Rotation
        //    transform.localRotation = OVRInput.GetLocalControllerRotation(Contorller);

     Vector2 stickData = OVRInput.Get(OVRInput.RawAxis2D.RThumbstick);

        stickData = OVRInput.Get(OVRInput.Axis2D.SecondaryThumbstick);

        //only need Y axis data for this
        float yData = stickData[1];
         Debug.Log(yData);
    //    Debug.Log(yData);
    //     transform.Rotate(  yData * rotateSpeed * Time.deltaTime,0,0);
        //     transform.Translate((Vector3.up * yData) * Time.deltaTime);
        
        Vector3 parent_up = gameObject.transform.root.TransformDirection(Vector3.up);
        Vector3 parent_fwd = gameObject.transform.root.TransformDirection(Vector3.forward);
        Vector3 current_up = transform.TransformDirection(Vector3.up);
        Vector3 current_fwd = transform.TransformDirection(Vector3.forward);


        float angle = 89;
        

     

        
        if(yData > 0)
        {
            //need to use differnet axis
            if (Vector3.Angle(parent_fwd, current_fwd) <= angle)

            {

                Debug.Log("a");
                transform.Rotate((Vector3.right * yData) * rotateSpeed * Time.deltaTime);
                
            }
            else
            {
                transform.Rotate((Vector3.right * -yData) * 2 * rotateSpeed * Time.deltaTime);

            }

        }
        if(yData < 0)
        {
            if (Vector3.Angle(parent_up, current_up) <= angle)

            {
                Debug.Log("b");
                transform.Rotate((Vector3.right * yData) * rotateSpeed * Time.deltaTime);
            
            }else
            {
                transform.Rotate((Vector3.right * -yData) *2* rotateSpeed * Time.deltaTime);

            }
        }



        if (Input.GetAxis(ButtonName) > .5f)
        {

      
           transform.root.rotation = Quaternion.Slerp(transform.root.rotation, OVRInput.GetLocalControllerRotation(Contorller), Time.deltaTime * 2.0f);
    
            //unable to limit to just side to side

            //  Debug.Log(OVRInput.GetLocalControllerRotation(Contorller));
        }
        

    }
}
