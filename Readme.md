# **Bronchoscopy VR game** #

*—Virtual Reality, video games proliferation and increasing acceptance of gamiﬁcation for training purposes allows us to develop alternative methods of computer based training.
 Bronchoscopy training beneﬁts from simulation techniques.
 Good practice in Bronchoscopy lowers associated patient risks.
 The author proposes development of a Bronchoscopy Virtual Reality Video Game to record,
 monitor and reinforce positive change in the behavior of medical students through repetitive points based scoring,
 interview,
 and rapid prototype development.*


```
#!python

 Repo Root		
	GlassLungVR	
		-Working Demo of work done to date
		-Includes touch controllers for movement
		-Imported blender asset of lung
		-Key movement script allowing for VR/Non-VR movement using generic Input
		-Rotation applied by right hand Gripper
		-Instructions appear in game
		-Targets spawned and gripper tested
		-Gripper currently disabled until animation is included
	Gripper action	
		-Work in Game logic for scoring mechanism & and Animation of imported gripper asset
	Surgery	
		-Main game logic test bed
	VR_ui	
		-test bed for new UI elements
	VR_room_demo	
		-general VR specific test room

```

 

## **Hardware Requirements** ##
** VR capable PC **

*  Graphics Card  -  NVIDIA GTX 1060 / AMD Radeon RX 480 or greater
*  Alternative Graphics Card - NVIDIA GTX 970 / AMD Radeon R9 290 or greater
*  CPU - Intel i5-4590 equivalent or greater
*  Memory - 8GB+ RAM
*  Video Output - Compatible HDMI 1.3 video output
*  USB Ports - 3x USB 3.0 ports plus 1x USB 2.0 port
*  OS - Windows 7 SP1 64 bit or newer

## Software Used ##
*  Unity3d - https://unity3d.com/
*  Blender - https://www.blender.org/
*  Photoshop - Adobe Photoshop cs6
*  Microsoft Visual Studio Community Edition - https://www.visualstudio.com/thank-you-downloading-visual-studio/?sku=Community&rel=15