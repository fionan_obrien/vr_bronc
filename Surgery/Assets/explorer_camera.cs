﻿using UnityEngine;
using System.Collections;

public class explorer_camera : MonoBehaviour {
    public float speed = 5.0F;
    public float rotationSpeed = 50.0F;
    public float horizontalSpeed = 1.0F;
    public float verticalSpeed = 1.0F;
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        float translation = Input.GetAxis("Vertical") * speed;
        float rotation = Input.GetAxis("Horizontal") * rotationSpeed;
        translation *= Time.deltaTime;
        rotation *= Time.deltaTime;
        transform.Translate(0, 0, translation);
        transform.Rotate(0, rotation, 0);
        float h = horizontalSpeed * Input.GetAxis("Mouse X");
        float v = verticalSpeed * Input.GetAxis("Mouse Y");
        transform.Rotate(-v, h, 0);
    }
}
