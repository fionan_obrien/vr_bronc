﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Qustionaire : MonoBehaviour {


    /*
     * This class is the main Questionaire class, it handles the visual look and logic of interaction of the VR questionaire
     * 
     * 
     * 
     * */

    private QuestionList list;
    public string AnswerFile;
    public string QuestionFile;
    public bool meep;
    public bool save = false;

    public string game_id = "1";

    public TextMesh Question_body;
    public TextMesh Possible_ans;
    public TextMesh Your_ans;

    public TextMesh nextLabel;
    public int linelength;

    private bool completed;
    System.IO.StreamWriter writer;

   private string ans;

	// Use this for initialization
	void Start () {
        writer = new System.IO.StreamWriter(AnswerFile, true);
        writer.AutoFlush = true;
        list = new QuestionList(QuestionFile);
        completed = false;
        GameStateController controller = FindObjectOfType<GameStateController>();

        game_id = controller.Name;


	}


    private void OnEnable()
    {
        OnNextQuestion.nextQuestion += OnNextQuestion_nextQuestion;
        QuestionList.questionsComplete += QuestionList_questionsComplete;
        GlowCube.update += GlowCube_update;
    }

    private void GlowCube_update(int value)
    {
        ans = "" + value;
    }

    private void OnDisable()
    {
        OnNextQuestion.nextQuestion -= OnNextQuestion_nextQuestion;
        QuestionList.questionsComplete -= QuestionList_questionsComplete;
        GlowCube.update -= GlowCube_update;
    }

    private void QuestionList_questionsComplete()
    {
        EndQuestionaire();
        completed = true;
    }

    private void OnNextQuestion_nextQuestion()
    {
        nextLabel.text = "Next";
        if (!completed)
        {

            if (list.current != null)
            {
                list.answerQuestion(ans);
                Save(ans);

            }
            else
            {

                list.getNextQuestion();
            }
            drawQuestion();
        }
    }

    private void EndQuestionaire()
    {
        writer.Close();
        Question_body.text = "Questionaire is now complete";
        Possible_ans.text = "Thank you";

    }

    void Save(string ans) {
        Debug.Log("Saving..");

         ans = "" + game_id + "," + list.current.print() + "," + ans;

        Debug.Log(ans);

        writer.WriteLine(ans);
   
        
    }
    void drawQuestion()
    {
      
        Question_body.text = wordwrap(list.current.print(), linelength);
        string labelContent = "";
        //correct spacing
        labelContent += spacing(Question_body.text);
        int num = 1;
        switch (list.current.type)
        {
            case Question.QuestionType.rating:
                
                foreach(string s in Question.ratings)
                {
                    labelContent +=  s + "\t";
                }

                break;
            case Question.QuestionType.binary:
                foreach (string s in Question.binary)
                {
                    labelContent += num++ + ": " + s + "\n";
                }
                break;
            case Question.QuestionType.qualitive:
                foreach (string s in Question.qualitives)
                {
                    labelContent += num++ + ": " + s + "\n";
                }
                break;
        }



        Possible_ans.text = labelContent;



    }

    string spacing(string s)
    {
        string val = "";
        for (int i = 0; i < s.Length; i++)
        {
            if (s[i] == '\n')
            {
                val += '\n';
            }
        }
        return val;
    }

    string wordwrap(string s ,int maxlength)
    {
        int runninngcharcount = 0;
        string[] words = s.Split(' ');

        string output = "";

        for (int i = 0; i < words.Length; i++)
        {

            string temp = words[i];

            if (temp.Length + runninngcharcount> maxlength)
            {

                output += '\n';
                runninngcharcount = temp.Length;

            }else
            {
                runninngcharcount += temp.Length;

            }
            output += " " + temp;
            runninngcharcount++;

        }





        return output;


    }

}
