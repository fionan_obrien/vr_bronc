﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotationCopy1axis : MonoBehaviour {

    /*
     * Test class for Stripping one axis of rotation from a complete rotation of a another object.
     * This same principle applies to the rotateCam script applied to the Main player object
     * 
     * */


    public GameObject other;

    Quaternion otherLast;
    float t;
    
	// Use this for initialization
	void Start () {
        otherLast = other.transform.rotation;
	}
	
	// Update is called once per frame
	void Update () {

        Quaternion otherCurrent = other.transform.rotation;

        float euls = other.transform.eulerAngles.z;

        Quaternion q = Quaternion.AngleAxis(euls, Vector3.forward);

        q = Quaternion.Slerp(transform.rotation, q, t);

 
        Debug.DrawLine(transform.position, transform.up);

        t += Time.deltaTime;

        transform.rotation = q;

	}
}
