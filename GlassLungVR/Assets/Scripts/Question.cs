﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Question  {



    /*
     * The question Object is used to sotre information about the Questions being asked in a Questionaire.
     * Questions have a enumerated type which affects their visual display and acceptable answers
     * 
     * Questions also store reference to other questions that may be required for them to be relevant
     * */

    bool isSeed;
    int[] parents;
    int number;
    string body;
    bool ans = false;
    public QuestionType type;

    public static string[] ratings = {"1","2","3","4","5" };
    public static string[] qualitives = { "disagree strongly","disagree","neither","agree","strongly agree"};
    public static string[] binary = { "yes","no" };

   public enum QuestionType {binary,rating,qualitive}

    public static int QuestionCount = 0;

    //Default assumes 1-5 rating
    public Question(string body,bool baseQuestion ) {

        this.number = QuestionCount++;
        this.body = body;
        this.parents = null;
        this.type = QuestionType.rating;
    
    }
    public Question(string body,QuestionType type, bool baseQuestion)
    {

        this.number = QuestionCount++;
        this.body = body;
        this.parents = null;
        this.type = type;
        
    }

    public Question(string body, int number, QuestionType type, bool ans, int[] parents)
    {

        this.body = body;
        this.number = number;
        this.type = type;
        this.ans = ans;
        this.parents = parents;
        QuestionCount++;
    }

   
    public static Question parseForQuestion(string s) {
        Debug.Log(QuestionCount +"");
        Question q = null;
        string[] data = s.Split(new string[] {","}, System.StringSplitOptions.RemoveEmptyEntries);

        List<string> data2 = new List<string>(s.Split(','));

        Debug.Log("data "+data.Length);
        Debug.Log("data1 "+data2.Count);
        string body = data[0];
        int number = QuestionCount; 
        QuestionType type;
        switch (data[1])
        {
            case "0":
                type = QuestionType.binary;
                break;
                    case "1":
                type = QuestionType.rating;
                break;
            case "2":
                type = QuestionType.qualitive;
                break;
            default:
                type = QuestionType.rating;
                break;

        }
        bool ans;
        if (data[2].Equals("TRUE")){
            ans = true;
        }else { ans = false; }


        List<int> list = new List<int>();

        for (int i = 3; i < data.Length; i++)
        {
            int p = -1;
            System.Int32.TryParse(data[i], out p);
            if (p != -1)
            {
                list.Add(p);
            }

        }
        q = new Question(body, number, type, ans, list.ToArray());

         
        return q;
    }
    public Question(string body, QuestionType type, bool response,params int[] parent) {

        this.number = QuestionCount++;
        this.body = body;
        ans = response;
        if (parent == null)
        {
            isSeed = true;
        }
        else
        {
            parents = parent;

        }
        this.type = type;
    }
    public bool evaluate(string ans1) {

        Debug.Log("ans1 = " + ans1 + "ans.tostring = " + ans.ToString());

        return ans1.Equals(ans.ToString());
    }
 
    
    public string print()
    {
        return this.body;

    }

    public bool isChildOf(int i) {
        if (parents == null) return false;
        foreach(int id in parents)
        {
            if( i == id) { return true; }
        }
        return false;
    } 

    public int  getId() { return this.number; }


}
