var searchData=
[
  ['oncompletecallback',['onCompleteCallBack',['../class_doxy_runner.html#ac1401822d6b3dea5626b786a94aa98d5',1,'DoxyRunner']]],
  ['one',['One',['../class_o_v_r_input_1_1_o_v_r_controller_base_1_1_virtual_button_map.html#a062323007bfc5b4983908db38a9bbdc6',1,'OVRInput.OVRControllerBase.VirtualButtonMap.One()'],['../class_o_v_r_input_1_1_o_v_r_controller_base_1_1_virtual_touch_map.html#abc6e69695aebf8b4db8d2a3d9ead4200',1,'OVRInput.OVRControllerBase.VirtualTouchMap.One()']]],
  ['optimalbuffersamplescount',['OptimalBufferSamplesCount',['../struct_o_v_r_plugin_1_1_haptics_desc.html#a72aa10deeb11bb3e69cddb6703493700',1,'OVRPlugin::HapticsDesc']]],
  ['orientation',['Orientation',['../struct_o_v_r_plugin_1_1_posef.html#ab5ff6003d3fadbbe1078dd81f1f08f99',1,'OVRPlugin.Posef.Orientation()'],['../struct_o_v_r_pose.html#aac81c38f4076ade5669058d402e6f1c9',1,'OVRPose.orientation()']]],
  ['origin',['origin',['../class_grow_links.html#ab439cd66975da1ce6e4e0fe2d993dfb6',1,'GrowLinks']]],
  ['other',['other',['../class_grip_movement.html#abc98c47cfc4df923760203f58d858225',1,'GripMovement.other()'],['../classrotation_copy1axis.html#a4f09388ebbbf93ffd882679635288448',1,'rotationCopy1axis.other()'],['../class_rotation_testing.html#ad77b62aa295ce631e500ae086004c7ca',1,'RotationTesting.other()']]],
  ['ovroverlayobj',['ovrOverlayObj',['../class_o_v_r_r_t_overlay_connector.html#a0af3010a29eb89282fd41a7d31d2f358',1,'OVRRTOverlayConnector']]]
];
