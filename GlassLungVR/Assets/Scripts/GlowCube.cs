﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlowCube : MonoBehaviour {

    /*
    * The Glow Cube represents an interactive cube that lights on touch. it is used as Interactive input to the Vr Questionaire 
    * 
    * 
    * */



    private TextMesh displayText;
    private static int GlowCubeCount = 0;
    private int value;

    public Material activeMat, inactiveMat;
    private bool isActive;
    private Renderer rend;

    public delegate void GlowCubeUpdate(int value);
    public static event GlowCubeUpdate update;

    // Use this for initialization
    void Start () {
        GlowCubeCount++;
        displayText=     this.GetComponentInChildren<TextMesh>();
        value = GlowCubeCount;
        displayText.text = GlowCubeCount + "";
        rend = this.GetComponent<Renderer>();
        Collider boxCollider = this.GetComponent<Collider>();
	}

    public void toggleActive()
    {
        this.isActive = !isActive;

        if (isActive)
        {
            rend.material = activeMat;

        }else
        {
            rend.material = inactiveMat;

        }

    }
    public void setActive(bool activeState) {
        isActive = activeState;
        if (isActive)
        {
            rend.material = activeMat;

        }
        else
        {
            rend.material = inactiveMat;

        }

    }




    public void OnTriggerEnter(Collider other)
    {
        Debug.Log("hey yo");
      //  toggleActive();
        update(value);
    }


    public static  void resetCounter()
    {

        GlowCubeCount = 0;

    }

    public int getValue() {
        return this.value;
    }


}
