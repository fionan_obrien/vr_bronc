﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnWallHit : MonoBehaviour {
    public delegate void WallHitAction(GameObject o,Vector3 position);
    public static event WallHitAction wallhit;

    /*
     *  Simplifed on wall hit event annouces to the Game that a collision has occured between the player and the Lung Wall
     * 
     * 
     * */


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "LungWall") { wallhit(collision.gameObject,this.transform.position); } else { Debug.Log(collision.gameObject.tag); }
    }
}
