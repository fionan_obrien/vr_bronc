﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class phantomLink : MonoBehaviour
{

    public bool linked;
    public GameObject nearestLink;
    public GameObject secondLink;


    public string nearest;
    public string secondnearest;

    public Color c1 = Color.yellow;
    public Color c2 = Color.red;
    public Color c3 = Color.blue;
    public Color c4 = Color.green;



    private LineRenderer primayLine ;


    private bool primaryOn, SecondaryOn;

    int framecount;
    public float beamLength;
    // Use this for initialization

    int layer;
    void Start()
    {
        gameObject.layer = 8;
        layer = LayerMask.NameToLayer("Phantom");
        transform.rotation = Quaternion.Euler(Vector3.zero);
     

        primayLine = gameObject.AddComponent<LineRenderer>();
        primayLine.useWorldSpace = true;
        primayLine.numPositions = 3;
        primayLine.material = new Material(Shader.Find("Particles/Additive"));
        primayLine.widthMultiplier = 0.2f;
        primayLine.SetPositions(new Vector3[] { Vector3.zero, Vector3.zero, Vector3.zero });
        primayLine.startColor = c1;
        primayLine.endColor = c3;
     

    }


    // Update is called once per frame
    void Update()
    {
        framecount++;


       

            GameObject[] objs = GameObject.FindGameObjectsWithTag("Phantom");


            if (objs.Length > 0)
            {

                for (int i = 0; i < objs.Length - 1; i++)
                {

                    float distance_a;
                    distance_a = Vector3.Distance(transform.position, objs[i].transform.position);

                    if (distance_a == 0) { continue; }
                    if (nearestLink == null || distance_a < Vector3.Distance(transform.position, nearestLink.transform.position))
                    {

                        secondLink = nearestLink;
                        nearestLink = objs[i];

                        nearest = nearestLink.transform.position.ToString();

                        if (secondLink != null)
                        {

                            secondnearest = secondLink.transform.position.ToString();
                        }
                    }


                }


            }

            if (nearestLink != null)
            {


                primayLine.SetPosition(0, transform.position);
                primayLine.SetPosition(1, nearestLink.transform.position);

            }



            if (secondLink != null)
            {
                //   primayLine.SetPosition(0, transform.position);
                primayLine.SetPosition(2, secondLink.transform.position);


            }
            else {
                primayLine.SetPosition(2, transform.position);

            }







        

    }
}