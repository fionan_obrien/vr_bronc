﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneShift : MonoBehaviour {

    /*
     *  This is a utility class enabling quick transition to next avaiable game scene
     * 
     * 
     * */
    public bool change = false;
    public delegate void GameEndAction();
    public static event GameEndAction GameOver;




    private void Update()
    {
        if (change) { change = false; NextLevel(); }
    }

    public static void NextLevel()
    {
      
        GameOver();
       
        UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex + 1);
       

    }
    public void next() {
        change = true;
    }

}
