﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitButton : MonoBehaviour {

    /*
     *  Exit Button scipt to move to next scene
     * 
     * */


  private  Collider boxCollider;
   public GameObject Master;
    private void Start()
    {
        boxCollider =  this.GetComponent<Collider>();
    }

    private void OnTriggerEnter(Collider other)
    {
       SceneShift ss =  Master.GetComponent<SceneShift>();

        ss.next();
    }
}
