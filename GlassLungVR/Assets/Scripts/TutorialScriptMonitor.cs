﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialScriptMonitor : MonoBehaviour {

    //added to a gameObject; and subscribed to the OnTutorialUpdateChange event this script enable or disables scripts given their script name


    private void OnEnable()
    {
        OnTutorialUpdateChange.tutUpdate += OnTutorialUpdateChange_tutUpdate;
    }
    private void OnDisable()
    {
        OnTutorialUpdateChange.tutUpdate -= OnTutorialUpdateChange_tutUpdate;
    }
    private void OnTutorialUpdateChange_tutUpdate(string s, bool b)
    {
        //grab each transform
        Transform[] ts = transform.GetComponentsInChildren<Transform>(true);

        foreach(Transform t  in ts) { 

        MonoBehaviour mono = (MonoBehaviour) t.GetComponent(s);

        if (mono != null) {
            mono.enabled = b;
            
        }
    }

    }
}
