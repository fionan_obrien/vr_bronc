﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class SpawnNode : MonoBehaviour {

    /*
     * This class randomly spawns a given amount of tagerget GameObjects given a list of acceptable locations.
     * 
     * 
     * */

    static ArrayList activeNodes = new ArrayList();
    static ArrayList availableNodes = new ArrayList();

    static bool firstRun = true;

    private  void OnEnable()
    {
        GameStart.gameStart += GameStart_gameStart;
    }
    private void OnDisable()
    {
        GameStart.gameStart -= GameStart_gameStart;
    }
    private void GameStart_gameStart()
    {
       if (firstRun)
      {
            firstRun = false;
            ActiveAtRandom(12);
       }
    }

    public delegate void TargetSpawned(Vector3 position,string name);
    public static event TargetSpawned targetSpawned;


    public  Rigidbody target;
    public bool active;
    public string location_name;
   

    private void Start()
    {

      //  this.name = location_name;
        availableNodes.Add(this);
    }

    public void toggle() {

        this.active = !this.active;
        if (active) {


            Rigidbody o2 = (Rigidbody) Instantiate(target, transform.position, transform.rotation,transform);

            activeNodes.Add(this);
            availableNodes.Remove(this);

           //for publishing event
            //targetSpawned(transform.position, location_name);
        }else
        {

            activeNodes.Remove(this);
            availableNodes.Add(this);

        }

    }

    static int countCalled = 0;

    public static void ActiveAtRandom(int number) 
    {
        Random rand = new Random();
   
        int count = 0;
        countCalled++;
      

        while(count< number && count <= availableNodes.Count - 1)
        {
  
           int index = (int)Random.Range(0,availableNodes.Count-1);

            SpawnNode node =(SpawnNode) availableNodes[index];

            node.toggle();
            count++;
        }

    }


}
