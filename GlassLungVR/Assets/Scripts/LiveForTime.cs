﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiveForTime : MonoBehaviour {

    /*
     * Script to dispose of gameObject given a short time.
     * This is usesful to not overload the system.
     * 
     * */


    public float lifetime = 10f;
    public GameObject target;
    private void Awake()
    {
        Destroy(target, lifetime);
    }

}
