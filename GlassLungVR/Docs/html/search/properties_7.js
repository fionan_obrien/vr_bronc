var searchData=
[
  ['id',['id',['../class_o_v_r_profile.html#a5e1e1d39643e77fd3d1a473de9c770fc',1,'OVRProfile']]],
  ['identity',['identity',['../struct_o_v_r_pose.html#ab1fe1f6d36ab555296ca5de73e485d6e',1,'OVRPose']]],
  ['instance',['instance',['../class_o_v_r_manager.html#a10c7c50d4c6e83d28e5ceea4bab67e35',1,'OVRManager']]],
  ['ipd',['ipd',['../class_o_v_r_profile.html#afa3a6092134531118e4300e647175bb9',1,'OVRProfile']]],
  ['isenabled',['isEnabled',['../class_o_v_r_tracker.html#a4c6b574a6a7cd0296066f3f8bd8a205f',1,'OVRTracker']]],
  ['isgrabbed',['isGrabbed',['../class_o_v_r_grabbable.html#af293dc881ce57d3a666031122a8c30f8',1,'OVRGrabbable']]],
  ['ishmdpresent',['isHmdPresent',['../class_o_v_r_manager.html#a564db1fc6953e6a1eca79820cc57299d',1,'OVRManager']]],
  ['ishswdisplayed',['isHSWDisplayed',['../class_o_v_r_manager.html#aa5a27b21518ab77ca887a9dd6c7299c2',1,'OVRManager']]],
  ['ispositiontracked',['isPositionTracked',['../class_o_v_r_tracker.html#a93a89e3ff9a017f5a9190ae8dbb0de53',1,'OVRTracker']]],
  ['ispowersavingactive',['isPowerSavingActive',['../class_o_v_r_manager.html#a706e7758e2cd724a649b48ae43bb895f',1,'OVRManager']]],
  ['ispresent',['isPresent',['../class_o_v_r_tracker.html#a3bd37b63fe2751178cd8f807700ce714',1,'OVRTracker']]],
  ['issupportedplatform',['isSupportedPlatform',['../class_o_v_r_manager.html#a2cf90ec494e4cf6bc466b848f3887919',1,'OVRManager']]],
  ['isuserpresent',['isUserPresent',['../class_o_v_r_manager.html#a5e16257dc5068ef2fb2a1c383977025d',1,'OVRManager']]]
];
