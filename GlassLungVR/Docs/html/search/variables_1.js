var searchData=
[
  ['b',['b',['../struct_o_v_r_plugin_1_1_colorf.html#ae36611a067d6e7b82299fa79b560e74f',1,'OVRPlugin::Colorf']]],
  ['back',['Back',['../class_o_v_r_input_1_1_o_v_r_controller_base_1_1_virtual_button_map.html#aa1f0962a3a54d3616dccc70945675426',1,'OVRInput::OVRControllerBase::VirtualButtonMap']]],
  ['backandsidedampen',['BackAndSideDampen',['../class_o_v_r_player_controller.html#ab296cb69afa2c2d2f27b744a67ee76cb',1,'OVRPlayerController']]],
  ['basefilestring',['BaseFileString',['../class_doxygen_window.html#a7a4acfac0a07a2a05f183e4f0bc53b62',1,'DoxygenWindow']]],
  ['binary',['binary',['../class_question.html#a4746900087165658c12f9ed88fcbbacd',1,'Question']]],
  ['boundarytype',['BoundaryType',['../struct_o_v_r_plugin_1_1_boundary_geometry.html#ade7df560c14754aebd7ffeba0e9d215c',1,'OVRPlugin::BoundaryGeometry']]],
  ['buttonname',['ButtonName',['../class_grab.html#a6314392ed916930b78fd3bd17bf4370b',1,'Grab.ButtonName()'],['../classrotate__scope.html#a0982724251cbaec2205a23d5d2fcb732',1,'rotate_scope.ButtonName()']]],
  ['buttons',['Buttons',['../struct_o_v_r_plugin_1_1_controller_state.html#ac5e0b61361f68f785c82fd6bfc179c8e',1,'OVRPlugin::ControllerState']]]
];
