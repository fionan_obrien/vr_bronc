﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GripMovementByVelocity : MonoBehaviour {


    /*
     * This script handles movement of the player
     * Requiriing both VR touch controllers to operate,
     *  this class handles the forward and backward movement based on the action of grip and movement via the left hand VR touch controller
     * 
     * To Move forward:

            Move your Left hand close to your right hand
            Hold down the grip button
            Move your left hand away from the right hand
            You will move forward in the direction you are facing
            Let go and repeat, backwards is just the reverse! 


    *   This class also releases routeplanner nodes that help calculate the current distance from the midpoint of the given tube.
     */



    public OVRInput.Controller handleContorller;
    public OVRInput.Controller tubeController;
    public bool Move_By_Grip = true;

    public GameObject routePlanner;
    public GameObject routePlannerX;

    //for publishing event
    public delegate void LeftGripAction(bool grip);
    public static event LeftGripAction leftgripAction;

    public delegate void NodePlaced(GameObject node);
    public static event NodePlaced placenode;

    public delegate void RemoveNode(GameObject node);
    public static event RemoveNode removenode;

    public int speed;
    public float trackingFidelity = 1.5f;
    public string gripMovementButton;

    private Vector3 handleVelocity, tubeVelocity;
    private float lastDistance;
    private float delta;
    private bool moveForward;

    //for backtracking
    private Stack<Vector3> PastPlace;
    private Vector3 lastPlace;
    private Vector3 origin;
    private int count;

    private bool gripping;


   private Transform mainCam;

    // Use this for initialization
    void Start () {
        PastPlace = new Stack<Vector3>();
        count = 0;
        lastPlace = transform.position;
        origin = transform.position;
        gripping = false;
        mainCam = Camera.main.transform;
    }
	
	// Update is called once per frame
	void Update ()
    {
        count++;
        getVelocity();

        //for publishing only
        bool check = isGripping();
        if (check != gripping) { leftgripAction(check); }
        gripping = check;


        if (gripping && Move_By_Grip)
        {
            //get controllers current velocity
            float distance = Vector3.Distance(lastPlace, transform.position);

            if (moveForward)
            {// if moving away from each other

                //ove this forward


                transform.position += mainCam.forward * Time.deltaTime * speed * delta;


                {
                    if (distance > trackingFidelity)
                    {

                        lastPlace = transform.position;
                        PastPlace.Push(lastPlace);

                        GameObject o = Object.Instantiate(routePlanner, lastPlace, transform.rotation);
                        //   placenode(o);

                    //    Debug.Log(transform.position + " last place" + PastPlace.Count);

                    }

                }
            }
            else //if moving closer
            {
                //if you get to the last palce

                    if (transform.position == lastPlace)
                    {
                        if (PastPlace.Count > 1)
                        {
                            lastPlace = PastPlace.Pop();

                            //    removenode(o);
                        }
                        else
                        {

                            lastPlace = origin;
                        }
                    }
                
                transform.position = Vector3.MoveTowards(transform.position, lastPlace, (Time.deltaTime * speed * delta));

            }

            count %= 60;
        }

       
    }
    bool isGripping() {  return Input.GetAxis(gripMovementButton) > .5f; }

       void getVelocity() {
      tubeVelocity = OVRInput.GetLocalControllerPosition(OVRInput.Controller.LTouch);
      handleVelocity=  OVRInput.GetLocalControllerPosition(OVRInput.Controller.RTouch);

        float currentDistance = Vector3.Distance(tubeVelocity, handleVelocity);
        moveForward = (currentDistance > lastDistance) ? true : false;

        delta = Mathf.Abs(currentDistance - lastDistance);
        delta *= 500;

        lastDistance = currentDistance;
        

    }
}
