﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MidPointGarner : MonoBehaviour {


    //for Event publication
    public delegate void DistanceFromMidpoint(float f);
    public static event DistanceFromMidpoint distanceFromMid;



    Vector3 origin;
    Vector3 up, down, left, right, forward, backward;
    Vector3[] checkVects;
    List<Vector3> contact_points;

    public float length = 10f;

    float distance;
	// Use this for initialization
	void Start () {

       origin = transform.position;

       checkVects = new Vector3[] {
           transform.up,
           transform.up * -1,
           transform.right * -1,
           transform.right,
           transform.forward,
           transform.forward * -1,
           Vector3.up,
           Vector3.down,
           Vector3.right,
           Vector3.left
       };

        contact_points = getWorkingSet(checkVects);

        //at least one hit found
        if (contact_points.Count > 1)
        {

            Vector3 midpoint = getMidpoint();
            distance  =Vector3.Distance(origin, midpoint);

            //publish
            distanceFromMid(distance);

        }

    }

   List<Vector3> getWorkingSet( Vector3[] array) {
        List<Vector3> set = new List<Vector3>();
        foreach (Vector3 v3 in array) {

           Vector3 v = checkSpokes(v3);

            if (v != origin)
            {
                set.Add(v);
            }
        }
        return set;

    }

    Vector3 getMidpoint() {

         Vector3 midpoint = Vector3.zero;
            foreach(Vector3 v3 in contact_points)
            {
                midpoint += v3;
            }

            midpoint /= contact_points.Count;

        return midpoint;
    }
    
    Vector3 checkSpokes(Vector3 x) {
        RaycastHit hit;
    
        if (Physics.Raycast(origin, x, out hit, length))
        {
            return hit.point;

        }else
        {

            return origin;
        }


    }


}
