﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatetoPlayerView : MonoBehaviour {

    public GameObject playerView;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (playerView != null)
        {
            this.transform.LookAt(playerView.transform);
            transform.Rotate(Vector3.up - new Vector3(0, 180, 0));
        }
	}
}
