﻿using UnityEngine;
using System.Collections;

public class Orb : MonoBehaviour {

    public float beamLength;
    public bool DEBUG;

	// Use this for initialization
	void Start () {
	
	}

    void drawGuiideLines() {

        Debug.DrawRay(transform.position, Vector3.down * beamLength);
        Debug.DrawRay(transform.position, Vector3.up * beamLength);
        Debug.DrawRay(transform.position, Vector3.forward * beamLength);
        Debug.DrawRay(transform.position, Vector3.back * beamLength);
        Debug.DrawRay(transform.position, Vector3.left * beamLength);
        Debug.DrawRay(transform.position, Vector3.right * beamLength, Color.red);
    }

	// Update is called once per frame
	void Update () {
        if (DEBUG) { drawGuiideLines();  }

        // use for sphere cast!!

        if (Physics.CheckSphere(transform.position, beamLength)){

            Collider[] hitcolliders = Physics.OverlapSphere(transform.position, beamLength);

            foreach (Collider hitCol in hitcolliders) {
                if (this.gameObject != hitCol.gameObject)
                {
                    //for each hit of this collider sphere
                    Debug.Log(hitCol.gameObject.name);

                }
            }

        }



    }
}
