﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallHitAudio : MonoBehaviour {

    /*
     * PLays audio on event of a wall hit.
     * 
     * */


    public AudioClip soundToPlay;
    public AudioSource audio;
    public float volume;


    private void OnEnable()
    {
        OnWallHit.wallhit += OnWallHit_wallhit;
    }
    private void OnDisable()
    {
        OnWallHit.wallhit -= OnWallHit_wallhit;
    }

    private void OnWallHit_wallhit(GameObject o,Vector3 pos)
    {
        audio.PlayOneShot(soundToPlay, volume);
    }

}
