﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotate_scope2 : MonoBehaviour {

    /*
     * 
     * This class allows for the rotation of the scope using the primary controller (default right)
     * The joysticks up and down axis provide the 90 degree rotation available in the Scope, rotation on the Y axis is stripped and interpolated form the primary hand location
     * 
     * 
     */



    public OVRInput.Controller Controller;
   
    public float rotateSpeed;
    public string FreeRoatateButton;
    public string joystickButton;
    public string resetViewButton;
    private float t;

    public delegate void LeverChangeAction(float direction, float degreeRot);
    public static event LeverChangeAction leverChange;

    public float Max_angle = 90f;


    // Use this for initialization

    private Quaternion startingRotation;

    private Transform mainCam;

    void Start () {
        t= 0f;
        mainCam = Camera.main.transform;
    }
	
	// Update is called once per frame
	void Update () {

        rotateByLever();
        rotateByGrip();


        if (checkResetButton())
        {
            resetRotationToDefault();

        }
    }


    void rotateByLever()
    {
        //get input from Joystick
        float yData = Input.GetAxis(joystickButton) ;

        //stop if value is nil
        if (yData == 0f) { return; }

        //invert as per actual bronchoscope
        yData *= -1;
        float angle;
        float amount = yData * rotateSpeed * Time.deltaTime;

        Quaternion q = transform.rotation;

        q *= Quaternion.Euler(Vector3.right * amount);
        
        //calculate angle in respect of root transform
        angle = Quaternion.Angle(transform.root.rotation, q);

        //limit to max angle (default 90) degrees of rotation
        if (angle > Max_angle) { Debug.Log("limit reached"); return; }

        //publish LeverChange event
        leverChange(yData,amount);

        //rotate accordingly
        transform.rotation = q;
    }


    void rotateByGrip() {

        if (Input.GetAxis(FreeRoatateButton) > .5f)
        {
            Quaternion otherCurrent = OVRInput.GetLocalControllerRotation(Controller);
            //strip the Y angle only
            float euls = otherCurrent.eulerAngles.y;
            Quaternion q = Quaternion.AngleAxis(euls, Vector3.forward);
            // calculate interopolation
            q = Quaternion.Slerp(transform.rotation, q, t);
            t += Time.deltaTime;
            //rotate root object accordingly
            transform.root.rotation = q;
        }
    }

    void resetRotationToDefault()
    {
        //simple method of removing the rotation
        transform.rotation = transform.root.rotation;


    }

    bool checkResetButton()
    {
        //TO-DO
        return false;

    }
}
