﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Indicators : MonoBehaviour {
    
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
       Vector3 forward = transform.TransformDirection(Vector3.forward) * 10;
    Vector3 up = transform.TransformDirection(Vector3.up) * 10;
    Vector3 left = transform.TransformDirection(Vector3.left) * 10;
    Vector3 right = transform.TransformDirection(Vector3.right) * 10;
        Debug.DrawRay(transform.position, forward, Color.green);
        Debug.DrawRay(transform.position, left, Color.red);
        Debug.DrawRay(transform.position, right, Color.blue);
        Debug.DrawRay(transform.position, up, Color.cyan);
    }
}
