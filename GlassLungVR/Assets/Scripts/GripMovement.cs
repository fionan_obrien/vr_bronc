﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GripMovement : MonoBehaviour {

    /*
     * Orignal GripMovement Scipt since replaced by GripMovmentByVelocity
     * 
     * */



    public GameObject other;
    public OVRInput.Controller Controller;

    public float speed;
    public string gripMovementButton;


    bool gripping;
    Vector3 lastPos, currentPos;



    private Stack<Vector3> PastPlace;
    private Vector3 lastPlace;
    private Vector3 origin;
    private int count;



    // Use this for initialization
    void Start () {
        lastPos = Vector3.zero;
        currentPos = Vector3.zero;

        PastPlace = new Stack<Vector3>();
        count = 0;
        lastPlace = transform.position;
        origin = transform.position;
    }
	
	// Update is called once per frame
	void Update () {

        gripping = isGripping();
        updateCurrentGripperPos();

        if (gripping)
        {

            float distance = Vector3.Distance(currentPos, lastPos);
            //    Debug.Log(distance + "travelled");
            Debug.Log("meep " + OVRInput.GetLocalControllerVelocity(OVRInput.Controller.LTouch));


            //if moving closer invert
            if (Vector3.Distance(transform.position,currentPos)< Vector3.Distance(transform.position, lastPos)) { distance *= -1; }

            transform.position += Camera.main.transform.forward * Time.deltaTime * speed * distance*1000;
            Debug.Log(Camera.main.transform.forward);

      

        }

	}

    private void updateCurrentGripperPos()
    {
        lastPos = currentPos;
        currentPos = OVRInput.GetLocalControllerPosition(Controller);


    }


    bool isGripping() {
        
        return Input.GetAxis(gripMovementButton) > .5f;

    }
    void moveForward() { }

    void moveBack() { }
}
