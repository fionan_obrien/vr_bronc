﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyMovement : MonoBehaviour {


    public float speed = 20;
    public bool sideKeyMovment = false;
    public float deadZOne = 0.1f;
    public float trackingFidelity = 1.5f;

   private Stack<Vector3> PastPlace;
   private Vector3 lastPlace;
   private  Vector3 origin;
   private  int count;
    


    // Use this for initialization
    void Start () {
		PastPlace = new Stack<Vector3>();
        count = 0;
        lastPlace = transform.position;
        origin = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        count++;

        float verticalMovement = Input.GetAxis("Vertical");

                if (verticalMovement > deadZOne) {
                 transform.position += Camera.main.transform.forward * Time.deltaTime * speed *verticalMovement;

            if(count == 60) {
                if (Vector3.Distance(lastPlace, transform.position) > trackingFidelity) {

                    lastPlace = transform.position;
                    PastPlace.Push(lastPlace);

                }
                
            }



                }else if (Input.GetAxis("Vertical") < -deadZOne)
        {
          //  transform.position -= Camera.main.transform.forward * Time.deltaTime * speed;


            if (Vector3.Distance(transform.position, lastPlace) <= trackingFidelity)
            {
                if (PastPlace.Count > 1) { 
                lastPlace = PastPlace.Pop();
                }else
                {

                    lastPlace = origin;
                }
            }
            
            transform.position = Vector3.MoveTowards(transform.position, lastPlace,(Time.deltaTime*speed*(verticalMovement*-1)));
        }


        if (sideKeyMovment) { 

        if (Input.GetAxis("Horizontal") > 0.5)
        {
            transform.position += Camera.main.transform.right * Time.deltaTime * speed;

        }
        else if (Input.GetAxis("Horizontal") < -0.5)
        {
            transform.position -= Camera.main.transform.right * Time.deltaTime * speed;

        }
        }
        count %= 60;

    }
}
