﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodePathController : MonoBehaviour {

   public GameObject node_a;
   public GameObject node_b;

   public GameObject midpointMaker;
 


    public GameObject wallhitnode;
    public GameObject middropnode;

    Vector3 collsion1;
    Vector3 collsion2;

    Vector3 node_point1, node_point2;


    bool changed;


    List<Vector3> path;
    List<Vector3> wallhits;
    List<Vector3> midpoints;
    Color[] colors = {Color.black,Color.blue,Color.cyan,Color.gray,Color.green,Color.magenta,Color.red,Color.yellow };
    int colNum = 0;

	// Use this for initialization
	void Start () {

        //Setup linerender for demonstration purposes
        path = new List<Vector3>();
        wallhits = new List<Vector3>();
        midpoints = new List<Vector3>();
     
        

        //setup path list
        path.Clear();
        Debug.Log(LayerMask.NameToLayer("wall"));
        path.Add(node_a.transform.position); //add node_a transform to path

        midpointMaker.transform.position = node_a.transform.position;//set maker to start postion





        //  refreshRays();
        /*
                while (!midpointMaker.transform.position.Equals(node_b.transform.position))
                { // do while midpointmaker postion != node_b position


                    RaycastHit hit;
                         Debug.Log("MidpointMaker not on Node b Moving..");

                    float remainingDistance = Vector3.Distance(midpointMaker.transform.position, node_b.transform.position);
                         Debug.Log("Distance from midpoint to end is " + remainingDistance.ToString());

                    if (Physics.Raycast(
                        midpointMaker.transform.position,
                        node_b.transform.position,
                        out hit,
                        remainingDistance * 1.1f
                        ))
                    {  // this casts a ray from the midpointmakers current postion, to the end position,only as long as a straight line distance,only on the wall layermask

                        //if collision 
                        collsion1 = hit.point;

                        if (hit.collider == node_b.GetComponent<Collider>())
                        {
                            midpointMaker.transform.position = node_b.transform.position; // breaking loop
                            path.Add(node_b.transform.position); // adding final node

                                 Debug.Log("Node B in sight! ending!");
                            break;

                        }
                        else
                        {
                            wallhits.Add(new Vector3(collsion1.x, collsion1.y, collsion1.z));

                                    Debug.Log("wall hit @ " + collsion1.ToString());
                            Vector3 incoming = hit.point - midpointMaker.transform.position;
                            Vector3 reflected = Vector3.Reflect(incoming, hit.normal); // get reflected vector

                                    Debug.Log("reflected line @ " + reflected.ToString());

                            if (Physics.Raycast(collsion1, reflected, out hit, 8))
                            {

                                collsion2 = hit.point;//get collision point 2
                                    Debug.Log("Collision2 detected @ " + collsion2.ToString());


                                Vector3 midpoint = (collsion1 + collsion2) / 2;//find midpoint of collision 1 and collision 2
                                     Debug.Log("midpoint calculated @ " + midpoint.ToString());

                                midpointMaker.transform.position = midpoint;     //set midpointmaker to this position
                                path.Add(midpoint);                              //add this postion to path.



                                     wallhits.Add(new Vector3(collsion2.x, collsion2.y, collsion2.z)); //***DEBUG
                                     midpoints.Add(new Vector3(midpoint.x, midpoint.y, midpoint.z));//***DEBUG
                                //TO-DO proximity rules


                            }
                            else
                            {

                                     Debug.Log("No Collision 2 detected!");
                                midpointMaker.transform.position = node_b.transform.position;     //set midpointmaker to this position
                                path.Add(node_b.transform.position);
                                //backup

                                break;
                            }

                        }

                    }
                    else //if  there is no colission add b to path and end
                    {
                        midpointMaker.transform.position = node_b.transform.position; // breaking loop
                        path.Add(node_b.transform.position); // adding final node
                        break;
                    }



                }
                */

        path = findPath(node_a.transform.position, node_b.transform.position);

        LineRenderer lineRenderer = gameObject.AddComponent<LineRenderer>();
        lineRenderer.material = new Material(Shader.Find("Particles/Additive"));
        lineRenderer.widthMultiplier = 0.2f;
       lineRenderer.numPositions = path.Count;
      //  lineRenderer.numPositions = 2;

        foreach (Vector3 v in midpoints)
        {


            placeNode(middropnode, v);
        }

        foreach (Vector3 v in wallhits)
        {

            placeNode(wallhitnode, v, colors[colNum % colors.Length]);
            colNum++;
        }

    }


    void refreshRays()
    {

        float orginalDistance = Vector3.Distance(midpointMaker.transform.position, node_b.transform.position); 

        while (!midpointMaker.transform.position.Equals(node_b.transform.position))
        { // do while midpointmaker postion != node_b position
            RaycastHit hit;
         //   Debug.Log("MidpointMaker not on Node b Moving..");

            float remainingDistance = Vector3.Distance(midpointMaker.transform.position, node_b.transform.position);
            // Debug.Log("Distance from midpoint to end is " + remainingDistance.ToString());
            Ray ray = new Ray(midpointMaker.transform.position,node_b.transform.position);

           Vector3 size = node_a.GetComponent<Collider>().bounds.size;

            float radius = size.x / 2;
           

            if (Physics.SphereCast(ray, radius, out hit, remainingDistance +radius))
            {  // this casts a ray from the midpointmakers current postion, to the end position,only as long as a straight line distance,only on the wall layermask

                //if collision 
                collsion1 = hit.point;
                Debug.Log(hit.collider.gameObject.name);
                if (hit.collider.tag == "node")
                {
                    midpointMaker.transform.position = node_b.transform.position; // breaking loop
                    path.Add(midpointMaker.transform.position); // adding final node

                    Debug.Log("Node B in sight! ending!");
                    break;

                }
                else if(hit.collider.tag =="wall")
                {
                    wallhits.Add(new Vector3(collsion1.x, collsion1.y, collsion1.z));

                    Debug.Log("wall hit @ " + collsion1.ToString());
                    Vector3 incoming = hit.point - midpointMaker.transform.position;
                    Vector3 reflected = Vector3.Reflect(incoming, hit.normal); // get reflected vector

                    Debug.Log("reflected line @ " + reflected.ToString());



                    ray = new Ray(collsion1, reflected);
                    if(Physics.SphereCast(ray, radius, out hit, orginalDistance + radius)){
                        collsion2 = hit.point;

                        Vector3 midpoint = (collsion1 + collsion2) / 2;
                        midpointMaker.transform.position = midpoint;     //set midpointmaker to this position
                        path.Add(midpoint);
                        wallhits.Add(new Vector3(collsion2.x, collsion2.y, collsion2.z)); //***DEBUG
                        midpoints.Add(new Vector3(midpoint.x, midpoint.y, midpoint.z));//***DEBUG
                    }
                    else
                    {
                        

                        Debug.Log("Error No second wall detected within range!");
                        midpointMaker.transform.position = collsion1 - reflected;  //set midpointmaker to this position
                        path.Add(midpointMaker.transform.position);
                        //backup

                        break;
                    }

                }

            }
            else //if  there is no colission add b to path and end
            {

                Debug.Log("Error no collision detected breaking loop");

                midpointMaker.transform.position = node_b.transform.position; // breaking loop
                path.Add(midpointMaker.transform.position); // adding final node
                break;
            }


        }
    }




        // Update is called once per frame
        void Update () {
       

        LineRenderer lineRenderer = GetComponent<LineRenderer>();
    
        Debug.Log("length of path is "+path.Count);
           lineRenderer.SetPositions(path.ToArray());

        //     lineRenderer.SetPosition(0, node_a.transform.position);
        //   lineRenderer.SetPosition(1, node_b.transform.position);
          Debug.DrawLine(node_a.transform.position, node_b.transform.position, Color.red);
        Debug.DrawRay(node_a.transform.position, node_b.transform.position-node_a.transform.position, Color.blue);

        //ebug.DrawLine(node_a.transform.position, node_a.transform.forward, Color.yellow);

        //  Debug.DrawLine(node_b.transform.position, node_a.transform.forward, Color.yellow);
        // Debug.DrawLine(midpointMaker.transform.position, node_a.transform.forward, Color.yellow);

        //Ray ray = new Ray(new Vector3(-2, 1.3f, -4), new Vector3(9.24f, 1, -8.1f));
        //Debug.DrawRay(new Vector3(-2, 1.3f, -4), new Vector3(9.24f, 1, -8.1f));


        foreach (Vector3 v in wallhits) {

            
        }

    }



    void placeNode(GameObject o,Vector3 here) {
        Object.Instantiate(o, here, o.transform.rotation);
    }
    void placeNode(GameObject o, Vector3 here,Color c)
    {
       GameObject whit =(GameObject) Object.Instantiate(o, here, o.transform.rotation);


        whit.GetComponent<Renderer>().material.color = c;

    }




    List<Vector3> findPath(Vector3 a,Vector3 b)
    {

        List<Vector3> path = new List<Vector3>();
        path.Add(a);

       

        Vector3 marker, first_Collision, second_Collision, reflected, midpoint,incoming;

        marker = a;

        float distance = Vector3.Distance(a, b);
        float distanceRemaining;

        RaycastHit hit;

        int firebreak = 0;
        float twiddle = 1;
        while (marker != b && firebreak !=1000)
        {
            firebreak++;
            Ray ray = new Ray(marker, b-marker);

            if (Physics.Raycast(ray, out hit))
                {
                log("ray cast hit something");

                if (hit.collider.tag == "node")
                {
                    log("raycast hit a node!");

                    marker = b;
                    path.Add(marker);
                    break;
                }
                else if (hit.collider.tag == "wall")
                {
                    log("raycast hit 1st wall");
                    
                    first_Collision = hit.point;
                    wallhits.Add(first_Collision);
                    incoming = b - marker;

                    reflected = Vector3.Reflect(incoming, hit.normal);
                  
                    ray = new Ray(first_Collision, reflected-first_Collision);

                   if( Physics.Raycast(ray, out hit)){
                        log("Raycast hit 2nd wall");
                        second_Collision = hit.point;

                        midpoint = (first_Collision + second_Collision) / 2;
                       
                        marker = midpoint;

                        path.Add(marker);

                        wallhits.Add(second_Collision);
                        midpoints.Add(midpoint);

                    }
                    else
                    {

                        log("raycast failed to hit second wall");
                        break;
                    }

                }else
                {

                    log("raycast hit something else");
                    break;
                }




            }else
            {
                log("Raycast failed to hit anything");
                //break from loop
                break;

            }

        }

        return path;
    }

    void log(string s) {

        Debug.Log(s);
    }


}
