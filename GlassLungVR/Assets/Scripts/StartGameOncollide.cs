﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGameOncollide : MonoBehaviour {

    /*script used to start game from start scene to Demo1*/

    public GameObject GameMangerObject;
    private void OnTriggerEnter(Collider other)
    {

        Debug.Log("Hit");
        if (GameMangerObject != null) {

            SceneShift shift = (SceneShift)GameMangerObject.GetComponent<SceneShift>();
            shift.next();
        }


    }

}
