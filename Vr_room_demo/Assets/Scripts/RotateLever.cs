﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateLever : MonoBehaviour {
  public  GameObject lever;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Vector2 stickData = OVRInput.Get(OVRInput.RawAxis2D.RThumbstick);

        OVRInput.Get(OVRInput.Axis2D.SecondaryThumbstick);

        //only need Y axis data for this
        float yData = stickData[1];

        transform.Rotate((Vector3.right * yData) * 50 * Time.deltaTime);
    }
}
