﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class linkDraw : MonoBehaviour {

    public GameObject phantom;
    LineRenderer line;
    
	// Use this for initialization
	void Start () {
       
        line = (LineRenderer)gameObject.GetComponent("LineRenderer");
	}
	
	// Update is called once per frame
	void Update () {
        if (phantom != null)
        {
            line.SetPosition(1, phantom.transform.position);
        }
	}
}
