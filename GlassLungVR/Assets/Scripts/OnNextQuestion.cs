﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnNextQuestion : MonoBehaviour {


    /*
     * Used in conjuction with the questionaire this class allows for progression through the available questions.
     * 
     * */


    private Collider colide;

    bool isActive;
	// Use this for initialization
	void Start () {
        isActive = true;
        colide = this.GetComponent<Collider>();
	}



    public delegate void NextQuestionRequested();
    public static event NextQuestionRequested nextQuestion;


    private void OnEnable()
    {
        GlowCube.update += GlowCube_update;
    }

    private void GlowCube_update(int value)
    {
        isActive = true;


    }

    private void OnTriggerEnter(Collider other)
    {
        if (isActive) {
            nextQuestion();
            isActive = !isActive;
        }
    }

    // Update is called once per frame
    void Update () {
		
	}



}
