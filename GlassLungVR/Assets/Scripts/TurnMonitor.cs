﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnMonitor : MonoBehaviour {

    /*
         This class  checks if a turn has occured. it checks the last node postion the current posion determines if a suitable distance and rotation have been applied 
         When a node has been placed it checks if this nodes position and rotation exceed that of a qualifying turn, and publishes this information
         */


   // for publishing
    public delegate void TurnPerformed(float angle);
    public static event TurnPerformed turnPerformed;

    private void OnEnable()
    {
        GripMovementByVelocity.placenode += GripMovementByVelocity_placenode;
    }
    private void OnDisable()
    {
        GripMovementByVelocity.placenode -= GripMovementByVelocity_placenode;
    }

    public float QualifyingAngle = 15f;
    public float QualifyingDistance = 5f;

    Vector3 position;
    Quaternion rotation;


    private void Start()
    {
        position = this.transform.position;
        rotation = this.transform.rotation;
    }
    

    private void GripMovementByVelocity_placenode(GameObject node)
    {
       if(Vector3.Distance(node.transform.position,position) > QualifyingDistance)
        {
            float angle = Quaternion.Angle(this.rotation, node.transform.rotation);
            if (angle > QualifyingAngle)
            {

                turnPerformed(angle);
            }
        }

        position = node.transform.position;
        rotation = node.transform.rotation;


    }

  


}
