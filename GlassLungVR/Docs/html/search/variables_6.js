var searchData=
[
  ['g',['g',['../struct_o_v_r_plugin_1_1_colorf.html#a735470563795895be0c196c0380b7909',1,'OVRPlugin::Colorf']]],
  ['game_5fid',['game_id',['../class_qustionaire.html#ae8b36550a69dd59c30c832a89835c438',1,'Qustionaire']]],
  ['game_5fnum',['game_num',['../class_game_log.html#a619211be8a449633f8f16e9108b0c134',1,'GameLog']]],
  ['gamemangerobject',['GameMangerObject',['../class_start_game_oncollide.html#aafc09060c898991fbbea8f8db5eb30e6',1,'StartGameOncollide']]],
  ['gamepad_5fpitchdegreespersec',['GamePad_PitchDegreesPerSec',['../class_o_v_r_debug_head_controller.html#a1c367117fcfe3a73f3244b4d86d61979',1,'OVRDebugHeadController']]],
  ['gamepad_5fyawdegreespersec',['GamePad_YawDegreesPerSec',['../class_o_v_r_debug_head_controller.html#aa4bdfbf6c681291ea1815ca29cdf44f6',1,'OVRDebugHeadController']]],
  ['glow_5fcube',['glow_cube',['../class_glow_cube_spawner.html#ae1aa78680954bb9254fe263f729cb822',1,'GlowCubeSpawner']]],
  ['grabbegin',['grabBegin',['../class_o_v_r_grabber.html#a73f250601d4ace3d0f82e22f62db979c',1,'OVRGrabber']]],
  ['grabend',['grabEnd',['../class_o_v_r_grabber.html#a338681eef30e75e7d619da33384cfca4',1,'OVRGrabber']]],
  ['grabmask',['grabMask',['../class_grab.html#a55aa1e0cad8d44502d2781c50d37fdc4',1,'Grab']]],
  ['grabradius',['grabRadius',['../class_grab.html#a5b41e728ae44f9429c17c7bc7a868285',1,'Grab']]],
  ['gravitymodifier',['GravityModifier',['../class_o_v_r_player_controller.html#a5f28824c01b830bbdd3cf2f8db08c750',1,'OVRPlayerController']]],
  ['gridkey',['GridKey',['../class_o_v_r_grid_cube.html#ad86e6a265640c7dd361b90b8ab06a4d2',1,'OVRGridCube']]],
  ['gripmovementbutton',['gripMovementButton',['../class_grip_movement.html#addbcbf9eff77a25019fcd250fc477442',1,'GripMovement.gripMovementButton()'],['../class_grip_movement_by_velocity.html#a2c100a333134fca27b18335cff841d93',1,'GripMovementByVelocity.gripMovementButton()']]],
  ['grippergameobject',['gripperGameObject',['../class_use_gripper_on_click.html#ab023bef606086c61e663566ac69d049b',1,'UseGripperOnClick']]]
];
