﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grab : MonoBehaviour {
    public float grabRadius;
    public LayerMask grabMask;
    public OVRInput.Controller controller;
    public string ButtonName;

    private GameObject grabbedObj;
    private bool grabbing;

    private Quaternion lastRotation, currentRotaion;

    void Grabing() {
        grabbing = true;
        RaycastHit[] hits;

        hits = Physics.SphereCastAll(transform.position, grabRadius, transform.forward, 0f, grabMask);

        if (hits.Length > 0) {
            int closestHit = 0;


            for (int i = 0;i < hits.Length ; i++)
            {
                if (hits[i].distance < hits[closestHit].distance) closestHit = i;
            }
            grabbedObj = hits[closestHit].transform.gameObject;
            grabbedObj.GetComponent<Rigidbody>().isKinematic = true;
            grabbedObj.transform.position = transform.position;
            grabbedObj.transform.parent = transform;

        }
    }

    void Drop() {
        grabbing = false;
        if (grabbedObj != null) {

            grabbedObj.transform.parent = null;
            grabbedObj.GetComponent<Rigidbody>().isKinematic = false;
            grabbedObj.GetComponent<Rigidbody>().velocity = OVRInput.GetLocalControllerVelocity(controller);

            grabbedObj.GetComponent<Rigidbody>().angularVelocity = GetAgnluarVelocity();

            grabbedObj = null;
        }

    }
    Vector3 GetAgnluarVelocity() {
        Quaternion deltaRot = currentRotaion * Quaternion.Inverse(lastRotation);
        return new Vector3(Mathf.DeltaAngle(0, deltaRot.eulerAngles.x), Mathf.DeltaAngle(0, deltaRot.eulerAngles.y), Mathf.DeltaAngle(0, deltaRot.eulerAngles.z));



    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (grabbedObj != null) {

            lastRotation = currentRotaion;
                currentRotaion = grabbedObj.transform.rotation;
        }
        

        if (!grabbing && Input.GetAxis(ButtonName) > .5) Grabing();
        if (grabbing && Input.GetAxis(ButtonName) < .5) Drop();
    }
}
