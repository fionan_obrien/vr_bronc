var searchData=
[
  ['vector2f',['Vector2f',['../struct_o_v_r_plugin_1_1_vector2f.html',1,'OVRPlugin']]],
  ['vector2i',['Vector2i',['../struct_o_v_r_plugin_1_1_vector2i.html',1,'OVRPlugin']]],
  ['vector3f',['Vector3f',['../struct_o_v_r_plugin_1_1_vector3f.html',1,'OVRPlugin']]],
  ['virtualaxis1dmap',['VirtualAxis1DMap',['../class_o_v_r_input_1_1_o_v_r_controller_base_1_1_virtual_axis1_d_map.html',1,'OVRInput::OVRControllerBase']]],
  ['virtualaxis2dmap',['VirtualAxis2DMap',['../class_o_v_r_input_1_1_o_v_r_controller_base_1_1_virtual_axis2_d_map.html',1,'OVRInput::OVRControllerBase']]],
  ['virtualbuttonmap',['VirtualButtonMap',['../class_o_v_r_input_1_1_o_v_r_controller_base_1_1_virtual_button_map.html',1,'OVRInput::OVRControllerBase']]],
  ['virtualneartouchmap',['VirtualNearTouchMap',['../class_o_v_r_input_1_1_o_v_r_controller_base_1_1_virtual_near_touch_map.html',1,'OVRInput::OVRControllerBase']]],
  ['virtualtouchmap',['VirtualTouchMap',['../class_o_v_r_input_1_1_o_v_r_controller_base_1_1_virtual_touch_map.html',1,'OVRInput::OVRControllerBase']]]
];
