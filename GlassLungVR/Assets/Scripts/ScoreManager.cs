﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour {

    /*
    * This class is used to calulcate and control the scoring metrics of the game.
    * It listens for a series of events and uses an internal plyaer class to record the score in accordance with these events
    * 
    * 
    * */
    public delegate void SaveScore(string playerName, string score);
    public static event SaveScore saveScore;


    private Player Current_Player;
    public string ActivePlayer = "Player 1";
    public int midpointVal = 100; 
    public int wallHitPenalty = 100;
    public int collectvalue = 500;

        

    
    private void OnEnable()
    {
        MidPointGarner.distanceFromMid += MidPointGarner_distanceFromMid;
        OnWallHit.wallhit += OnWallHit_wallhit;
        TurnMonitor.turnPerformed += TurnMonitor_turnPerformed;
        GameStart.gameStart += GameStart_gameStart;
        SceneShift.GameOver += SceneShift_GameOver;
        GripperCollector.gripperCollect += GripperCollector_gripperCollect;

    }

    private void GripperCollector_gripperCollect()
    {
        Current_Player.Add(collectvalue);
    }

    private void SceneShift_GameOver()
    {
        saveScore(Current_Player.getPlayerName(),Current_Player.getScore());
    }

    private void GameStart_gameStart()
    {
        GameStateController controller = FindObjectOfType<GameStateController>();

        Current_Player = new Player(controller.Name);
       
    }

    private void OnDisable()
    {
        MidPointGarner.distanceFromMid -= MidPointGarner_distanceFromMid;
        OnWallHit.wallhit -= OnWallHit_wallhit;
        TurnMonitor.turnPerformed -= TurnMonitor_turnPerformed;
        GameStart.gameStart -= GameStart_gameStart; 
        SceneShift.GameOver -= SceneShift_GameOver;
        GripperCollector.gripperCollect -= GripperCollector_gripperCollect;
    }

    private void MidPointGarner_distanceFromMid(float f)
    {

        if (f == 0f)
        {
            Current_Player.Add(midpointVal);
        } else {

            //calculate linear cost of being off midpoint
            float val = midpointVal / Mathf.Pow(f, 2);

            Current_Player.Add(val);
 
                }
    }


    private void OnWallHit_wallhit(GameObject o,Vector3 position)
    {

        Current_Player.subtract(wallHitPenalty);

    }
    private void TurnMonitor_turnPerformed(float angle)
    {
        float val = Mathf.Pow(angle,2);
        val /= midpointVal;
        Current_Player.Add(val);
    }



  public  class Player
    {

        public delegate void OnScoreUpdate(string score);
        public static event OnScoreUpdate ScoreUpdate;

        static int gameNumber = getGameNumber();

        private static int getGameNumber()
        {

            //To-Do
            return 0;
        }

        string name;
        int score;
        int currentGameNumber;
        Stack<Vector3> collisions;

        public string getPlayerName() { return this.name; }
        public Player(string name) {
            this.name = name;
            this.score = 0;
            this.collisions = new Stack<Vector3>(); 
            gameNumber++;
            this.currentGameNumber = gameNumber;
        }


        public void Add(float x)
        {
            score +=(int) x;
            ScoreUpdate(""+score);
        }
        public void subtract(float x) {

            score -=(int) x;
        }

        public void addWallHit(Vector3 pos) {
            collisions.Push(pos);
        }

        public string getScore() { return "" + score; }

    }

}
