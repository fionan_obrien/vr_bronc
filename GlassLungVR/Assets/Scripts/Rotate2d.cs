﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate2d : MonoBehaviour {

    /*
     * Used for simple constant rotation motion on 2 axis
     * 
     * */


    public Vector3 direction = Vector3.up;
    public float speed = .5f;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Quaternion q = transform.rotation;

 
        Quaternion q2 = Quaternion.AngleAxis(speed, direction);
     

        q = q  * q2 ;


        transform.rotation = q;


    }
}
