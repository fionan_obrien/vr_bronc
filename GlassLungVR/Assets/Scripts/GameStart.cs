﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStart : MonoBehaviour {


    /*
     * Utility Class to annouce the GameStart Event
     * 
     * */

    public delegate void GameStartAction();
    public static event GameStartAction gameStart;


    private void Start()
    {
        if (gameStart != null) {
            gameStart();
        }
    }


}
