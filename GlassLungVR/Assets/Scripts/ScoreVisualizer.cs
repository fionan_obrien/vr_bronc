﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreVisualizer : MonoBehaviour {

    TextMesh mesh;

    private void Start()
    {
        mesh = this.GetComponent<TextMesh>();
    }


    private void OnEnable()
    {
        ScoreManager.Player.ScoreUpdate += Player_ScoreUpdate;
    }
    private void OnDisable()
    {
        ScoreManager.Player.ScoreUpdate -= Player_ScoreUpdate;
    }

    private void Player_ScoreUpdate(string score)
    {
        mesh.text = score;

    }
}
