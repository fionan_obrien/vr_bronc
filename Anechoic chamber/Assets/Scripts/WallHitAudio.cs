﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallHitAudio : MonoBehaviour {


    public AudioClip soundToPlay;
    public AudioSource audio;
    public float volume;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnTriggerEnter(Collider other)
    {
        audio.PlayOneShot(soundToPlay, volume);
    }
}
