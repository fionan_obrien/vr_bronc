﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppearOnNear : MonoBehaviour {

    /*
     * This class allows one object to monitor another objects proximity, if close enough it appears.
    */

    public float proximity;

    private MeshRenderer render;

    public GameObject subject;

    private Transform subjectTransfrom;
    

    // Use this for initialization

	void Start () {
        render = gameObject.GetComponentInChildren<MeshRenderer>();

        if(subject == null) {

            subjectTransfrom = Camera.main.transform;
        }else
        {
            subjectTransfrom = subject.transform;

        }

	}
	
	// Update is called once per frame
	void Update () {

        if (Vector3.Distance(subjectTransfrom.position,transform.position)<proximity) {
            render.enabled = true;
        }else { render.enabled = false; }


	}
}
