﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GripperAnimController : MonoBehaviour
{

    /*
     * Animation Scipt for Gripper Animation
     * 
     * */

    Animator anim;

    public delegate void OnGripperCompleted();
    public static event OnGripperCompleted gripperComplete;

    private void Start()
    {
        anim = this.GetComponent<Animator>();
    }


    private void OnEnable()
    {
        UseGripperOnClick.gripperActionPress += UseGripperOnClick_gripperActionPress;
    }

    private void OnDisable()
    {
        UseGripperOnClick.gripperActionPress -= UseGripperOnClick_gripperActionPress;
    }
    private void UseGripperOnClick_gripperActionPress()
    {


        anim.SetBool("BiopsyClawDown",true);

    }
}
